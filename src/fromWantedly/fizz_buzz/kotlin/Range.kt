package fromWantedly.fizz_buzz.kotlin

// In order to operate at AND
class Range(private val min: Int, private val max: Int) {

    private val nAlias: MutableList<Als> = mutableListOf()

    fun addAlias(info:String) = this.nAlias.add(Als(info))

    fun addCondition(aliasInfo: String, target: Cndt) {
        for (als in this.nAlias) {
            if (als.info == aliasInfo) // should i use === ???
                als.nCndt.add(target)
                break
        }
    }

    fun editedList(): String {
        val list = StringBuilder()

        for (target in this.min..this.max) {
            var isNumber = true

            for (als in this.nAlias) {
                if (als.rewritten(target, list)) isNumber = false
            }

            if (isNumber) list.append(target)

            list.appendln()
        }

        return list.toString()
    }

}


fun main(args: Array<String>) {


    var range   = Range(1, 100)

    var info01  = "Fizz"
    var info02  = "Buzz"

    range.addAlias(info01)
    range.addAlias(info02)

    println("------- Basic -------")
    range.addCondition(info01, Mltpl(3))
    range.addCondition(info02, Mltpl(5))

    println(range.editedList())


    println("------- Apply -------")
    range.addCondition(info01, Incldd(3))
    range.addCondition(info02, Incldd(5))

    println(range.editedList())


    println("------- Apply 2 -------")
    range = Range(1,100)
    range.addAlias("MMJ")
    range.addCondition("MMJ", Incldd(9))

    println(range.editedList())

}